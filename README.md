## vue3-element-plus-admin-vite
> 基于 vite5.x 重构整个前端项目，封装更多场景化组件...正在完善中

基于 vite5.x + vue3.x + element-plus2.x + axios1.x 的后台管理系统基础模板

## 功能特性
- 使用 Vue 3 和 Vite 构建，具备高效的开发体验和优秀的性能表现。
- 集成了 Element Plus 组件库，提供美观的 UI 界面和丰富的组件选项 和主题切换。
- 使用 Vue Router 实现路由管理，支持多层级路由和动态路由。
- 使用 Axios 处理 HTTP 请求，与后端进行数据交互。
- 集成 ECharts 图表库，展示数据统计和可视化效果。
- 集成 Pinia 状态管理库，它是 Vue 的专属状态管理库，它允许跨组件或页面共享状态。

## 安装使用
- 获取项目代码
```bash
git clone https://gitee.com/daboyunali/vue3-element-plus-admin-vite.git
```
- 进入项目目录
```bash
cd vue3-element-plus-admin-vite
```
- 安装依赖
```bash
yarn --registry=https://registry.npm.taobao.org/

yarn install
```
- 启动服务
```bash
yarn dev
```
- 构建测试环境
```bash
yarn build:stage
```
- 构建生产环境
```bash
yarn build:prod
```
- 前端访问地址
```bash
http://localhost:80
```

## 贡献
欢迎对 vue3-element-plus-admin-vite 项目的改进和完善，如果您发现任何错误或有任何建议，请随时提交问题和拉取请求。
## 版权
vue3-element-plus-admin-vite 是一个开源项目，根据Apache许可证发布。有关更多信息，请参阅LICENSE文件。
