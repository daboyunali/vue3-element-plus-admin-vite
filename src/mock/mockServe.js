//引入mockjs
import Mock from 'mockjs'

//引入JSON数据格式
import captchaImage from './jsonData/login/captchaImage.json'
import login from './jsonData/login/login.json'
import getInfo from './jsonData/login/getInfo.json'
import getRouters from './jsonData/login/getRouters.json'
import systemMenuList from './jsonData/system/menu/systemMenuList.json'
import sys_show_hide from './jsonData/system/dict/sys_show_hide.json'
import sys_normal_disable from './jsonData/system/dict/sys_normal_disable.json'

//mock数据  参数：请求地址，请求方法，请求数据
Mock.mock("/api/captchaImage", 'get', captchaImage)
Mock.mock("/api/login", 'post', login)
Mock.mock("/api/getInfo", 'get', getInfo)
Mock.mock("/api/getRouters", 'get', getRouters)
Mock.mock(RegExp('/api/system/menu/list.*'), 'get', ({url}) => {
  // 获取？后面的参数
  const query = url.indexOf('?') !== -1 && url.split('?')[1]
  // 解析参数
  const queryStr = new URLSearchParams(query)
  const menuName = queryStr.get('menuName')
  const status = queryStr.get('status')
  let queryList = []
  if (menuName && status) {
    systemMenuList && systemMenuList.forEach((item) => {
      if (item.menuName.indexOf(menuName) !== -1 && item.status === status) {
        queryList.push(item)
      }
    })
  } else if (menuName) {
    systemMenuList && systemMenuList.forEach((item) => {
      if (item.menuName.indexOf(menuName) !== -1) {
        queryList.push(item)
      }
    })
  } else if (status) {
    systemMenuList && systemMenuList.forEach((item) => {
      if (item.status === status) {
        queryList.push(item)
      }
    })
  } else {
    queryList = systemMenuList
  }
  return {
    "msg": "操作成功",
    "code": 200,
    "data": queryList
  }
})
Mock.mock(RegExp('/api/system/menu/.*'), 'get', ({url}) => {
  // 获取？后面的参数
  const query = url.split('?')[1]
  // 解析参数
  const queryStr = new URLSearchParams(query)
  let id
  if (query) {
    id = queryStr.get('id')
  } else {
    id = url.split('/')[url.split('/').length - 1]
  }
  const index = systemMenuList.findIndex(item => item.menuId === Number(id))
  let arr = systemMenuList.splice(index, 1)[0]
  return {
    "msg": "操作成功",
    "code": 200,
    "data": arr
  }
})
Mock.mock(RegExp('/system/dict/data/type/.*'), 'get', ({url}) => {
  const type = url.split('/')[url.split('/').length - 1]
  switch (type) {
    case 'sys_show_hide':
      return sys_show_hide
    case 'sys_normal_disable':
      return sys_normal_disable
    default:
      break
  }
})
