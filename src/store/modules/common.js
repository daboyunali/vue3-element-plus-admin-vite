// 在组件setup函数内直接使用
export const useCommonState = defineStore({
  id: 'useCommonState',
  /**
   * state 被定义为一个返回初始状态的函数
   */
  state: () => ({}),
  /**
   * Getter 完全等同于 store 的 state 的计算值
   */
  getters: {
    // getUserById: (state) => {
    //   return (userId) => state?.count + userId
    // },
  },
  /**
   * Action 相当于组件中的 method
   */
  actions: {},
});

// Pinia 热更新，确保传递正确的 store 声明
if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useCommonState, import.meta.hot))
}
