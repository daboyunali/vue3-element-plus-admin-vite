import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import {useCommonState} from "@/store/modules/common";

// 创建
const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)

const registerStore = () => {
  return {
    pinia: pinia,
    commonState: useCommonState(pinia),
  };
};
// 在组件setup函数外都可以使用
const store = registerStore();

export default store
